//
//  SHNWebViewController.h
//  hackernews
//
//  Created by Sami Aref on 12/23/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHNWebViewController : UIViewController

@property (nonatomic, strong) NSURL *url;

@end
