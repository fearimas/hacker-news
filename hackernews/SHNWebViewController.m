//
//  SHNWebViewController.m
//  hackernews
//
//  Created by Sami Aref on 12/23/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import "SHNWebViewController.h"

@interface SHNWebViewController ()

@end

@implementation SHNWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIWebView *webView = (UIWebView *)self.view;
    webView.scalesPageToFit = YES;
    [webView loadRequest:[NSURLRequest requestWithURL:self.url]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
