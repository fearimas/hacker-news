//
//  SHNCommentsViewController.m
//  hackernews
//
//  Created by Sami Aref on 12/22/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import "SHNCommentsViewController.h"
#import "SHNWebViewController.h"
#import "SHNCommentViewCell.h"

@interface SHNCommentsViewController () <SHNCommentViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, strong) NSArray *allComments;

@end

@implementation SHNCommentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	[[HNManager sharedManager] loadCommentsFromPost:self.post completion:^(NSArray *comments)
     {
         for (HNComment *comment in comments)
         {
             comment.height = [SHNCommentViewCell heightForText:comment.Text
                                                          level:comment.Level];
         }
         
         self.allComments = comments;
         self.comments = [comments mutableCopy];
         [self.collectionView reloadData];
     }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.comments.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(320, CGFLOAT_MAX);
    HNComment *comment = self.comments[indexPath.item];
    //    CGFloat padding = comment.Level > 0 ? 0.0f : 20.0f;
    size.height = MAX(50, comment.collapsed ? 0 : comment.height + 30.0f + 2.0f);
    return size;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SHNCommentViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.comment = self.comments[indexPath.item];
    cell.delegate = self;
    
    return  cell;
}

- (void)commentCellDidSelectURL:(NSURL *)url
{
    SHNWebViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WebVC"];
    webVC.url = url;
    webVC.title = url.absoluteString;
    
    [self.navigationController pushViewController:webVC animated:YES];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HNComment *comment =  self.comments[indexPath.item];
    
    int level = comment.Level;
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSMutableArray *comments = [NSMutableArray array];
    
    comment.collapsed = !comment.collapsed;
    
    if (!comment.collapsed)
    {
        int index = [self.allComments indexOfObject:comment];
        int j = indexPath.item + 1;
        
        for (int i = index + 1; i < self.allComments.count; i++)
        {
            HNComment *comment = self.allComments[i];
            if (comment.Level == level) break;
            [comments addObject:comment];
            [indexPaths addObject:[NSIndexPath indexPathForItem:j inSection:0]];
            [self.comments insertObject:comment atIndex:j];
            j++;
        }
    }
    else
    {
        for (int i = indexPath.item + 1; i < self.comments.count; i++)
        {
            HNComment *comment = self.comments[i];
            if (comment.Level <= level) break;
            [comments addObject:comment];
            [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
        }
        
        [self.comments removeObjectsInArray:comments];
    }
    
    [self.collectionView performBatchUpdates:^
     {
         comment.collapsed ? [self.collectionView deleteItemsAtIndexPaths:indexPaths] : [self.collectionView insertItemsAtIndexPaths:indexPaths];
         [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
     }
                                  completion:^(BOOL finished)
     {
         
     }];
    
    
}

@end
