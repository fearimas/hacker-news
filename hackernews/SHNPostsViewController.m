//
//  SHNFrontPageViewController.m
//  hackernews
//
//  Created by Sami Aref on 12/5/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import "SHNPostsViewController.h"
#import "HNManager.h"
#import "SHNCommentsViewController.h"
#import "SHNWebViewController.h"

@interface SHNPostsViewController () <UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSArray *filters;
@property (nonatomic) NSInteger filterIndex;
@property (nonatomic, strong) NSMutableArray *posts;
@property (nonatomic, strong) NSString *baseUrlString;
@property (nonatomic, weak) UIRefreshControl *refreshControl;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *barItem;
@property (nonatomic) CGFloat height;
@property (nonatomic) BOOL loading;

@end

@implementation SHNPostsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.posts = [NSMutableArray array];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refreshControl;
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    self.filters = @[@"Top",@"Ask",@"New",@"Jobs",@"Best"];
    
    [self refreshData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.height = self.view.frame.size.height;
    self.barItem.title = self.filters[self.filterIndex];
}



- (void)fetchPosts
{
    if (self.loading) return;
    
    self.loading = YES;
    
    if ([HNManager sharedManager].postUrlAddition)
    {
        [[HNManager sharedManager] loadPostsWithUrlAddition:[HNManager sharedManager].postUrlAddition completion:^(NSArray *posts)
         {
             [self loadPosts:posts append:YES];
         }];
    }
    else
    {
        [[HNManager sharedManager] loadPostsWithFilter:self.filterIndex completion:^(NSArray *posts)
         {
             [self loadPosts:posts append:NO];
         }];
    }
}

- (void)loadPosts:(NSArray *)posts append:(BOOL)append
{
    [self.refreshControl endRefreshing];
    self.loading = NO;
    
    if (!posts.count) return;
    
    if (!append)
    {
        [self.posts removeAllObjects];
    }
    
    [self.posts addObjectsFromArray:posts];
    
    for (HNPost *post in posts)
    {
        post.height = [self heightForText:post.Title];
    }
    
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    
}

- (void)refreshData
{
    [HNManager sharedManager].postUrlAddition = nil;
    [self.refreshControl beginRefreshing];
    [self fetchPosts];
}

- (CGFloat)heightForText:(NSString *)text
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:18]};
    
    return ceilf([text boundingRectWithSize:CGSizeMake(265,CGFLOAT_MAX)                                              options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:attributes
                                    context:nil].size.height);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HNPost *post = self.posts[indexPath.item];
    
    SHNWebViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WebVC"];
    
    webVC.url = [NSURL URLWithString:post.UrlString];
    webVC.title = post.Title;
    
    [self.navigationController pushViewController:webVC animated:YES];
}

- (IBAction)tappedButton:(id)sender
{
    HNPost *post = self.posts[[self.collectionView indexPathForCell:[sender superview].superview.superview].item];
    id vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsVC"];
    [vc setPost:post];
    [vc setTitle:post.Title];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    CGRect navBarFrame = navBar.frame;
    navBarFrame.origin.y = 20.0f;
    
    CGRect frame = self.view.frame;
    frame.origin.y = 64.0f;
    frame.size.height = self.height;
    
    [UIView animateWithDuration:0.f animations:^
     {
         navBar.frame = navBarFrame;
         self.view.frame = frame;
         navBar.topItem.titleView.alpha = 1.0f;
         
     }];
}

- (IBAction)tappedPostFilter:(UIBarButtonItem *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Close"]];
    closeButton.momentary = YES;
    closeButton.backgroundColor = [UIColor whiteColor];
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.tintColor = self.navigationController.navigationBar.tintColor;
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    [pickerView selectRow:self.filterIndex inComponent:0 animated:NO];
    
    [actionSheet addSubview:pickerView];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    actionSheet.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];
    
}

- (void)dismissActionSheet:(id)sender
{
    UIActionSheet *actionSheet = (UIActionSheet *)[sender superview]; [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    self.barItem.title = self.filters[self.filterIndex];
    [self refreshData];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.filters.count;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return self.filters[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.filterIndex = row;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint scrollVelocity = [scrollView.panGestureRecognizer velocityInView:self.view];
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    
    if (scrollVelocity.y > 0.0f)
    {
        CGRect navBarFrame = navBar.frame;
        navBarFrame.origin.y = 20.0f;
        
        CGRect frame = self.view.frame;
        frame.origin.y = 64.0f;
        frame.size.height = self.height;
        
        [UIView animateWithDuration:0.1f animations:^
         {
             navBar.frame = navBarFrame;
             self.view.frame = frame;
             navBar.topItem.titleView.alpha = 1.0f;
             self.barItem.title = self.filters[self.filterIndex];
         }];
    }
    else if (scrollVelocity.y < 0.0f)
    {
        CGRect navBarFrame = navBar.frame;
        navBarFrame.origin.y = -24.0f;
        
        CGRect frame = self.view.frame;
        frame.origin.y = 20.0f;
        frame.size.height = self.height + 44;
        
        [UIView animateWithDuration:0.1f animations:^
         {
             navBar.frame = navBarFrame;
             self.view.frame = frame;
             navBar.topItem.titleView.alpha = 0.0f;
             self.barItem.title = @"";
         }];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(320, CGFLOAT_MAX);
    HNPost *story = self.posts[indexPath.item];
    size.height = MAX(50, story.height + 25 + 5);
    
    return size;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.posts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    HNPost *story = self.posts[indexPath.item];
    
    if (story == self.posts.lastObject)
    {
        [self fetchPosts];
    }
    
    UILabel *title = [cell viewWithTag:10];
    title.text = story.Title;
    CGRect frame =  title.frame;
    frame.size.height = story.height;
    title.frame = frame;
    
    UIButton *commentCount = [cell viewWithTag:20];
    NSString *text = [NSString stringWithFormat:@"%d",story.CommentCount];
    [commentCount setTitle:text forState:UIControlStateNormal];
    
    UILabel *points = [cell viewWithTag:30];
    points.text = [NSString stringWithFormat:@"%d",story.Points];
    
    UILabel *urlLabel = [cell viewWithTag:40];
    
    NSString *host = [[NSURL URLWithString:story.UrlString] host];
    urlLabel.text = host;
    
    UILabel *nameLabel = [cell viewWithTag:50];
    nameLabel.text = story.Username;
    
    [points sizeToFit];
    [urlLabel sizeToFit];
    [nameLabel sizeToFit];
    
    urlLabel.center = CGPointMake(CGRectGetMidX(urlLabel.bounds) + CGRectGetMaxX(points.frame) + 3.0f,
                                  urlLabel.center.y);
    nameLabel.center = CGPointMake(CGRectGetMidX(nameLabel.bounds) + CGRectGetMaxX(urlLabel.frame) + 3.0f,
                                   nameLabel.center.y);
    
    return  cell;
}


@end
