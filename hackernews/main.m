//
//  main.m
//  hackernews
//
//  Created by Sami Aref on 12/5/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SHNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SHNAppDelegate class]));
    }
}
