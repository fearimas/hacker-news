//
//  SHNCommentsViewController.h
//  hackernews
//
//  Created by Sami Aref on 12/22/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HNManager.h"

@interface SHNCommentsViewController : UICollectionViewController

@property (nonatomic, strong) HNPost *post;

@end
