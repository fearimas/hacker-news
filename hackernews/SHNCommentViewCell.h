//
//  SHNCommentViewCell.h
//  hackernews
//
//  Created by Sami Aref on 1/8/14.
//  Copyright (c) 2014 Sami Aref. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HNComment.h"
#import "HNCommentLink.h"

@protocol SHNCommentViewCellDelegate <NSObject>

- (void)commentCellDidSelectURL:(NSURL *)url;

@end


@interface SHNCommentViewCell : UICollectionViewCell

+ (CGFloat)heightForText:(NSString *)text level:(int)level;

@property (nonatomic, strong) HNComment *comment;
@property (nonatomic, weak) id<SHNCommentViewCellDelegate> delegate;

@end
