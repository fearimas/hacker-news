//
//  SHNAppDelegate.h
//  hackernews
//
//  Created by Sami Aref on 12/5/13.
//  Copyright (c) 2013 Sami Aref. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
