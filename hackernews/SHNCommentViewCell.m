//
//  SHNCommentViewCell.m
//  hackernews
//
//  Created by Sami Aref on 1/8/14.
//  Copyright (c) 2014 Sami Aref. All rights reserved.
//

#import "SHNCommentViewCell.h"
#import "TTTAttributedLabel.h"

static CGFloat kIndentation = 25.0f;
static CGFloat kWidth = 285.0f;

@interface SHNCommentViewCell () <TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *collapsedArrow;
@end

@implementation SHNCommentViewCell

+ (CGFloat)heightForText:(NSString *)text level:(int)level
{
    CGFloat width = kWidth - (level * kIndentation);
    
    NSDictionary *attributes = @{NSFontAttributeName:[self font]};
    
    return ceilf([text boundingRectWithSize:CGSizeMake(width,CGFLOAT_MAX)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:attributes
                                    context:nil].size.height);
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tintColor = [UIColor colorWithRed:277.0f / 255.0f
                                     green:92.0f / 255.0f
                                      blue:57.0f / 255.0f
                                     alpha:1];
    
    self.commentLabel.font = [SHNCommentViewCell font];
    
    self.commentLabel.linkAttributes = @{
                                         (id)kCTForegroundColorAttributeName : self.tintColor,
                                         (id)kCTUnderlineStyleAttributeName : @(kCTUnderlineStyleSingle)
                                          };
    
    self.commentLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.commentLabel.delegate = self;
}

- (void)setComment:(HNComment *)comment
{
    _comment = comment;
    
    self.commentLabel.text = comment.Text;
    
    CGRect frame = self.containerView.frame;
    frame.origin.x = 5 + comment.Level * kIndentation;
    frame.size.height = comment.height + 30.0f;
    self.containerView.frame = frame;
    self.containerView.alpha = comment.collapsed ? 0.4 : 1.0f;
    
    CGFloat width = kWidth - (comment.Level * kIndentation);
    frame =  self.commentLabel.frame;
    frame.size.height = comment.height;
    frame.size.width = width;
    self.commentLabel.frame = frame;
    
    self.nameLabel.text = comment.Username;
    [self.nameLabel sizeToFit];
    
    self.dateLabel.text = comment.TimeCreatedString;
    
    CGFloat x = CGRectGetMidX(self.dateLabel.bounds) + CGRectGetMaxX(self.nameLabel.frame) + 3.0f;
    self.dateLabel.center = CGPointMake(x, self.dateLabel.center.y);
    
    self.collapsedArrow.text = !comment.collapsed ?  @"▾"  : @"▸" ;
}

+ (UIFont *)font
{
    static UIFont *font;
    
    if (!font)
    {
        font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    }
    
    return font;
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [self.delegate commentCellDidSelectURL:url];
}

@end
